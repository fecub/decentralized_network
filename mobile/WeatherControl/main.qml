import QtQuick 2.10
import QtQuick.Controls 2.2
import "qrc:/WeatherControl/" as WeatherControl

ApplicationWindow {
    visible: true
    width: WeatherControl.Constants.width
    height: WeatherControl.Constants.height
    title: qsTr("Tabs")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {
            id:_page1
        }

        Page2Form {
            id:_page2
        }

        Page3Form {
            id:_page3

        }
        Page4Form {
            id:_page4

        }
        onCurrentIndexChanged: {
            if (!_page1.activity)
                _page1.background.color="white"
            if (!_page2.activity)
                _page2.background.color="white"
            if (!_page3.activity)
                _page3.background.color="white"
            if (!_page4.activity)
                _page4.background.color="white"
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: "\uf00d"
            font.family: WeatherControl.Constants.fontWeatherFamily
            font.pixelSize: Qt.application.font.pixelSize * WeatherControl.Constants.icontabsize
        }
        TabButton {
            text: "\uf050"
            font.family: WeatherControl.Constants.fontWeatherFamily
            font.pixelSize: Qt.application.font.pixelSize * WeatherControl.Constants.icontabsize
        }
        TabButton {
            text: "\uf078"
            font.family: WeatherControl.Constants.fontWeatherFamily
            font.pixelSize: Qt.application.font.pixelSize * WeatherControl.Constants.icontabsize
        }
        TabButton {
            text: "\uf079"
            font.family: WeatherControl.Constants.fontWeatherFamily
            font.pixelSize: Qt.application.font.pixelSize * WeatherControl.Constants.icontabsize
        }
    }
}
