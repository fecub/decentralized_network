import QtQuick 2.10
import QtQuick.Controls 2.2
import "qrc:/WeatherControl/" as WeatherControl
import io.yazcub.network 1.0

Page {
    id: pageroot
    width: 600
    height: 400
    property bool activity

    header: Label {
        text: qsTr("Photovoltaik")
        font.pixelSize: Qt.application.font.pixelSize * 3
        font.capitalization: Font.AllUppercase
        font.family: "Comfortaa"
        anchors.top:parent.top
        anchors.topMargin: 50 * WeatherControl.Constants.scale_factor
        padding: 10
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        text:"\uf00d"
        anchors.centerIn: parent
        font.family: WeatherControl.Constants.fontWeatherFamily
        font.pixelSize: Qt.application.font.pixelSize * 15
    }
    UdpSocket {
        id: uSock
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(!activity) {
                pageroot.background.color = "#fcffb5"
                console.log("starte sonne")
                activity=true
                uSock.sendUdp(1)
            } else {
                pageroot.background.color = "#fff"
                console.log("beende sonne")
                activity=false
                uSock.sendUdp(1)
            }
        }
    }
}
