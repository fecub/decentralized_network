import QtQuick 2.10
import QtQuick.Controls 2.2
import "qrc:/WeatherControl/" as WeatherControl
import io.yazcub.network 1.0

Page {
    id: pageroot
    width: 600
    height: 400
    property bool activity

    header: Label {
        text: qsTr("Wasserkraftwerk")
        font.pixelSize: Qt.application.font.pixelSize * 3
        font.capitalization: Font.AllUppercase
        font.family: "Comfortaa"
        anchors.top:parent.top
        anchors.topMargin: 50 * WeatherControl.Constants.scale_factor
        padding: 10
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        text:"\uf078"
        anchors.centerIn: parent
        font.family: WeatherControl.Constants.fontWeatherFamily
        font.pixelSize: Qt.application.font.pixelSize * 15
    }

    UdpSocket {
        id: uSock
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(!activity) {
                pageroot.background.color = "#cae7f7"
                console.log("starte wasser")
                activity=true
                uSock.sendUdp(3)
            } else {
                pageroot.background.color = "#fff"
                console.log("beende wasser")
                activity=false
                uSock.sendUdp(3)
            }
        }
    }
}
