QT += quick network
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    network.cpp

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
#QML_IMPORT_PATH+=/home/fecub/workspace/decentralized_network/mobile/WeatherControl/imports
#QML_IMPORT_PATH=$$PWD
#QML_IMPORT_PATH+=/home/fecub/workspace/decentralized_network/mobile/WeatherControl/imports/WeatherControl

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

#importPaths:["imports/"]

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    imports/WeatherControl/Comfortaa/Comfortaa-Bold.ttf \
    imports/WeatherControl/Comfortaa/Comfortaa-Light.ttf \
    imports/WeatherControl/Comfortaa/Comfortaa-Regular.ttf \
    imports/WeatherControl/Lato/Lato-Black.ttf \
    imports/WeatherControl/Lato/Lato-BlackItalic.ttf \
    imports/WeatherControl/Lato/Lato-Bold.ttf \
    imports/WeatherControl/Lato/Lato-BoldItalic.ttf \
    imports/WeatherControl/Lato/Lato-Hairline.ttf \
    imports/WeatherControl/Lato/Lato-HairlineItalic.ttf \
    imports/WeatherControl/Lato/Lato-Italic.ttf \
    imports/WeatherControl/Lato/Lato-Light.ttf \
    imports/WeatherControl/Lato/Lato-LightItalic.ttf \
    imports/WeatherControl/Lato/Lato-Regular.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-Bold.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-BoldItalic.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-ExtraBold.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-ExtraBoldItalic.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-Italic.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-Light.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-LightItalic.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-Regular.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-SemiBold.ttf \
    imports/WeatherControl/Open_Sans/OpenSans-SemiBoldItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Black.ttf \
    imports/WeatherControl/Poppins/Poppins-BlackItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Bold.ttf \
    imports/WeatherControl/Poppins/Poppins-BoldItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-ExtraBold.ttf \
    imports/WeatherControl/Poppins/Poppins-ExtraBoldItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-ExtraLight.ttf \
    imports/WeatherControl/Poppins/Poppins-ExtraLightItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Italic.ttf \
    imports/WeatherControl/Poppins/Poppins-Light.ttf \
    imports/WeatherControl/Poppins/Poppins-LightItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Medium.ttf \
    imports/WeatherControl/Poppins/Poppins-MediumItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Regular.ttf \
    imports/WeatherControl/Poppins/Poppins-SemiBold.ttf \
    imports/WeatherControl/Poppins/Poppins-SemiBoldItalic.ttf \
    imports/WeatherControl/Poppins/Poppins-Thin.ttf \
    imports/WeatherControl/Poppins/Poppins-ThinItalic.ttf \
    imports/WeatherControl/Signika/Signika-Bold.ttf \
    imports/WeatherControl/Signika/Signika-Light.ttf \
    imports/WeatherControl/Signika/Signika-Regular.ttf \
    imports/WeatherControl/Signika/Signika-SemiBold.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-Black.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-Bold.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-BoldItalic.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-ExtraLight.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-ExtraLightItalic.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-Italic.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-Light.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-LightItalic.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-Regular.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-SemiBold.ttf \
    imports/WeatherControl/Titillium_Web/TitilliumWeb-SemiBoldItalic.ttf \
    imports/WeatherControl/fa-brands-400.ttf \
    imports/WeatherControl/fa-regular-400.ttf \
    imports/WeatherControl/fa-solid-900.ttf \
    imports/WeatherControl/Comfortaa/OFL.txt \
    imports/WeatherControl/Open_Sans/LICENSE.txt \
    imports/WeatherControl/Poppins/OFL.txt \
    imports/WeatherControl/Signika/OFL.txt \
    imports/WeatherControl/Titillium_Web/OFL.txt \
    imports/WeatherControl/qmldir \
    imports/WeatherControl/Constants.qml \
    imports/WeatherControl/Descriptions.qml \
    images/wind.png \
    imports/WeatherControl/fontawesome-webfont.ttf \
    imports/WeatherControl/weathericons-regular-webfont.ttf \
    WeatherControl/Comfortaa/Comfortaa-Regular.ttf

HEADERS += \
    network.h
