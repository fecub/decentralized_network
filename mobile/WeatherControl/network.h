#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include <QThread>

class UdpThread : public QThread
{
    Q_OBJECT

protected:
    void run();

public:
    QString Typ() const;
    void setTyp(const QString &Typ);

    bool Threadrunning() const;
    void setThreadrunning(bool Threadrunning);

private:
    QUdpSocket *m_Udpc;
    QString m_Typ;
    bool m_Threadrunning;
};


class Network : public QObject
{
    Q_OBJECT

public:
    explicit Network(QObject *parent = nullptr);

public slots:
    void sendUdp(QString typ);

private:
    bool threadactiv;
    UdpThread _uThread;
};

#endif // NETWORK_H
