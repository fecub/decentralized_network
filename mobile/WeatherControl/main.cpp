#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFontDatabase>
#include "network.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<Network>("io.yazcub.network", 1, 0, "UdpSocket");

    if(QFontDatabase::addApplicationFont(QStringLiteral(":/WeatherControl/Comfortaa/Comfortaa-Regular.ttf")) == -1)
        qDebug() << "Failed to load font Comfortaa";


    QQmlApplicationEngine engine;
    engine.addImportPath("/home/fecub/workspace/decentralized_network/mobile/WeatherControl/imports");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
