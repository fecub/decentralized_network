#include "network.h"


void UdpThread::run()
{
    m_Udpc = new QUdpSocket(this);
    m_Udpc->bind(QHostAddress::Broadcast, 8000);
    while(m_Threadrunning) {
        QString datagram = "LOAD_WEATHER,"+Typ();
        QByteArray barray = datagram.toLatin1();
        m_Udpc->writeDatagram(barray.data(), QHostAddress::Broadcast, 8000);

        qDebug() << datagram;
        QThread::sleep(2);
    }
}


QString UdpThread::Typ() const
{
    return m_Typ;
}

void UdpThread::setTyp(const QString &Typ)
{
    m_Typ = Typ;
}

bool UdpThread::Threadrunning() const
{
    return m_Threadrunning;
}

void UdpThread::setThreadrunning(bool Threadrunning)
{
    m_Threadrunning = Threadrunning;
}

Network::Network(QObject *parent) : QObject(parent)
{
    threadactiv = false;
    //    _uThread = UdpThread();
}

void Network::sendUdp(QString typ) {
    qDebug() << threadactiv;
    if(!threadactiv) {
        threadactiv = true;
        _uThread.setTyp(typ);
        _uThread.setThreadrunning(true);
        _uThread.start();
    }
    else {
        _uThread.setThreadrunning(false);
        _uThread.quit();
        _uThread.setTyp("");
        threadactiv = false;
    }
}
