# Decentralized networking 
A system which can work decentralized. Example usage for this, virtual power plant

## 3 energy types
- Photovoltaik
- Water
- Bio
- Wind


####* Notes:
**kill command:**
```
  kill -9 $(ps aux | grep '[p]ython server.py' | awk '{print $2}')

```

**android speed**
```
To run dex in process, the Gradle daemon needs a larger heap.
It currently has 1024 MB.
For faster builds, increase the maximum heap size for the Gradle daemon to
at least 1536 MB.
To do this set org.gradle.jvmargs=-Xmx1536M in the project gradle.properties.
For more information see
https://docs.gradle.org/current/userguide/build_environment.html
```

