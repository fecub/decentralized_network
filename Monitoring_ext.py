#!/usr/bin/venv python
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from socket import SOL_SOCKET, SO_BROADCAST

from random import randint
import argparse
import sys
import os
import time

class Monitoring(DatagramProtocol):
    """docstring for Monitoring."""
    def __init__(self, port, machineid):
        self.protocolSoc = None
        self.port = port
        self.machineid = machineid
        self.charge_activ = False
        # self.send()

    def startProtocol(self):
        self.transport.socket.setsockopt(SOL_SOCKET, SO_BROADCAST, True)

    def sendDatagram(self, data):
        liveping = "Data: " + str(os.getpid())+' - '+ data
        self.transport.write(liveping, ('255.255.255.255', 8000)) # <- write to broadcast address here
        time.sleep(1)

    def datagramReceived(self, datagram, host):
        print 'Datagram received: ', repr(datagram)
        if 'CHARGE_NOT' in datagram:
            self.charge_activ = False
        if 'CHARGE_AUTHORIZED' in datagram:
            self.charge_activ = True
            self.send()
        if 'CHARGE_CONTINUE' in datagram:
            self.send()
        

    def send(self):
        # while True:
        if (self.charge_activ):
            self.sendDatagram("CHARGE,{0},6".format(self.machineid))
        else:
            self.sendDatagram("CHARGE_REQ,{0}".format(self.machineid))
        # time.sleep(1)
        #0 means any port

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Monitoring system for decentralized server')
    parser.add_argument('-p', '--port', type=str, help='Port number', required=True, nargs='+')
    parser.add_argument('-m', '--machine', type=str, help='machine id', required=True, nargs='+')
    args = parser.parse_args()
    port = int(args.port[0].split(",")[0])
    machineid = args.machine[0]

    mon = Monitoring(port, machineid)
    # mon.init_devices()
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()

    t = reactor.listenUDP(port, mon)
    mon.send()
    reactor.run()
