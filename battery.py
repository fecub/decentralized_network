#!venv/bin/python

from threading import Thread
from random import randint
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from socket import SOL_SOCKET, SO_BROADCAST
import os
import time
import sys
import pickle
IOT_DEVICE = "armv7l"
if IOT_DEVICE in os.uname()[-1]:
    import battery_led as BatteryDisplay

MEDIUM_LOAD   = 50
CRITIC_LOAD   = 10
BATTERY_EMPTY = 0
RUNNINGBATCHECK = True

sys.dont_write_bytecode = False


class BatteryClass(Thread, DatagramProtocol):
    def __init__(self, port, machineid):
        self.port = port
        self.machineid = machineid
        self.battery_load = self.loadfromdisk()
        self.secondsToSleep = None
        self.charge_from_foreign_stat = False
        self.temp_load = 0
        self.charge_status = None
        
        self.charge_own_activ = False
        self.charge_foreign_activ = False
        self.charge_req_current = []


        if IOT_DEVICE in os.uname()[-1]:
            BatteryDisplay.init()
        
    ####################
    ##### NETWORKING
    ####################
    def startProtocol(self):
        self.transport.socket.setsockopt(SOL_SOCKET, SO_BROADCAST, True)

    def sendDatagram(self, data):
        liveping = "NOTIFY,{0},{1}".format(data, self.port)
        self.transport.write(liveping, ('255.255.255.255', 8000)) # <- write to broadcast host here
        time.sleep(1)

    def datagramReceived(self, datagram, host):
        print 'Datagram received: ', repr(datagram)
        if 'CHARGE_NOT' in datagram:
                self.charge_foreign_activ = False
                self.charge_own_activ = False
                # self.charge_req_current = []
                del self.charge_req_current[:]
                print "CHARGE not"
            
        if 'CHARGE_REQ' in datagram:
            # print self.charge_own_activ
            # print self.charge_foreign_activ
            if (not self.charge_own_activ and not self.charge_foreign_activ):
                self.charge_own_activ = True
                self.charge_req_current = datagram.split(',')
                print "host: {0}".format(host)
                tmp=list(host)
                tmp[1] = int(datagram.split(',')[-1])
                host = tuple(tmp)
                print "host: {0}".format(host)
                self.transport.write("CHARGE_AUTHORIZED", host)
                return True
            
            # self.charge_own_activ = False
            # self.transport.write("CHARGE_NOT", host)
        
        # print('''self.charge_req_current: {0}
        # type(self.charge_req_current):{1}
        # datagram.split(","): {2}'''.format(self.charge_req_current, 
        #                                     type(self.charge_req_current), 
        #                                     datagram.split(',')))
        
        if(len(self.charge_req_current) > 1):
            # print datagram.split(',')
            if ('CHARGE' in datagram and
                self.charge_req_current[1] == datagram.split(',')[1] and
                self.charge_battery_before(int(datagram.split(',')[2]), host) and 
                self.charge_own_activ):
                self.transport.write("CHARGE_CONTINUE", host)
                print "CHARGE own"
            else:
                self.transport.write("CHARGE_NOT", host)
                del self.charge_req_current[:]
                self.charge_foreign_activ = False

                
        if 'CHARGE_AUTHORIZED' in datagram:
            self.charge_foreign_activ = True
            self.send_charge_battery(host)
            print "CHARGE authorized"

        if 'CHARGE_CONTINUE' in datagram:
            self.send_charge_battery(host)
            print "CHARGE continuing"

    ####################
    ##### THREADING
    ####################
    def init_thread(self):
        Thread.__init__(self)
    
    def run(self):
        global RUNNINGBATCHECK

        while(RUNNINGBATCHECK):
            if (self.temp_load < self.battery_load):
                self.charge_status = 'CHARGING'
            else: 
                self.charge_status = 'UNCHARGING'
            print("{0} {1}: BAT STATUS = {2}%").format(self.charge_status, self.getName(), self.battery_load)
 
            # Sleep for random time between 1 ~ 3 second
            self.secondsToSleep = randint(5, 10)

            self.temp_load = self.battery_load
            self.checks()
            time.sleep(self.secondsToSleep)

    def stop(self):
        global RUNNINGBATCHECK
        RUNNINGBATCHECK = False

    ###############
    ##### METHODS
    ###############
    def charge_battery_before(self, value, address):
        if(self.charge_battery(value)):
            return True
        self.transport.write("CHARGE_NOT", address)
        self.charge_own_activ = False
        return False

    def send_charge_battery(self, address):
        # print self.get_battery_status()
        print "send_charge_battery method: {0}".format(address)
        if self.get_battery_status() > 30:
            print("request charging to " + str(address))
            if (self.charge_foreign_activ):
                self.transport.write("CHARGE,{0},6".format(self.machineid), address )
                print "CHARGE foreign"
            else:
                self.transport.write("CHARGE_REQ,{0}".format(self.machineid), address)
                print "CHARGE_REQ"
        else:
            self.charge_foreign_activ = False
            # self.charge_req_current = []
            self.transport.write("CHARGE_NOT", address)   
            del self.charge_req_current[:]
        time.sleep(2)

    def checks(self):
        # battery handling; means we are consuming battery
        self.consume_battery()

        # checking battery status
        if self.check_load() is not None:
            # print self.check_load()[0]
            # print self.check_load()
            if self.check_load()[0] in (-1,-2):
                if 'UNCHARGING' in self.charge_status:
                    print "sending datagram"
                    self.sendDatagram(self.check_load()[1])
    
    def consume_battery(self):
        self.loadfromdisk()

        if (self.battery_load > self.secondsToSleep):
            self.battery_load = self.battery_load - self.secondsToSleep
        elif (self.secondsToSleep > self.battery_load):
            self.battery_load = 0

        self.writetodisk()

    def charge_battery(self, value):
        self.loadfromdisk()
        if (self.battery_load >= 90):
            return False
        self.battery_load = self.battery_load + value
        self.writetodisk()
        return True

    def charge_without_limit(self, value):
        self.loadfromdisk()
        if(self.battery_load + value > 100 and self.battery_load < 100):
            value = 100 - self.battery_load
        if(self.battery_load >= 100):
            return 
        self.battery_load = self.battery_load + value
        self.writetodisk()
        return True

    def check_load(self):
        if (self.battery_load > MEDIUM_LOAD):
            return 1, 'FULL_LOAD'
        elif (self.battery_load < MEDIUM_LOAD and self.battery_load > CRITIC_LOAD):
            return 0, 'MEDIUM_LOAD'
        elif (self.battery_load < CRITIC_LOAD and self.battery_load > BATTERY_EMPTY):
            return -1, 'CRITIC_LOAD'
        elif (self.battery_load == BATTERY_EMPTY):
            return -2, 'BATTERY_EMPTY'

    ####################
    ##### getter
    ####################
    def get_battery_status(self):
        self.loadfromdisk()
        return self.battery_load
    
    ####################
    ##### FILE HANDLE
    ####################
    def loadfromdisk(self):
        try:
            with open("batstat.p", "r") as f:
                self.battery_load = int(pickle.load(f))
        except IOError:
            self.battery_load = 100
            self.writetodisk()

    def writetodisk(self):
        if IOT_DEVICE in os.uname()[-1]:
            BatteryDisplay.setBatteryLevelPercent(self.battery_load)
        
        with open("batstat.p", "wb") as f:
            pickle.dump( str(self.battery_load), f) #, protocol=pickle.HIGHEST_PROTOCOL
    
