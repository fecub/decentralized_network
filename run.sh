#!/bin/bash

#kill -9 $(ps aux | grep 'run.sh' | awk '{print $2}')
#kill -9 $(ps aux | grep '[p]ython server.py' | awk '{print $2}')
#venv/bin/python server.py -p 8002 -m maclux
jobs -s | kill -9 %1
runvenv() {
    kill -9 $(ps aux | grep '[p]ython server.py' | awk '{print $2}')
    venv/bin/python server.py -p $port -m $machine -e $energy
}

runnormal() {
    kill -9 $(ps aux | grep '[p]ython server.py' | awk '{print $2}')
    python server.py -p $port -m $machine -e $energy
}

USAGE="script usage: $(basename $0) [-p port example: -p 8002] [-m machine
example: -m maclux] [-v virtualenv(true:1,false:0)  example: -v 1] [-e energy example: -e 1] [-k kill
run.sh example: -k]" >&2

if [ $# -eq 0 ] ; then
    echo $USAGE
    exit 0
fi

port=""
machine=""
virtualenv=""
energy=""

while getopts ':p:m:v:e:k' OPTION; do
  case "$OPTION" in
    p)
      port="$OPTARG"
      ;;
    m)
      machine="$OPTARG"
      ;;
    v)
      virtualenv="$OPTARG"
      ;;
    e)
      energy="$OPTARG"
      ;;
    k)
      kill -9 $(ps aux | grep 'run.sh' | awk '{print $2}')
      ;;
    ?)
      echo $USAGE
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

echo $port
echo $machine
echo $virtualenv
echo $energy

if [ ! -z "$port" ] &&  [ ! -z "$machine" ] &&  [ "$virtualenv" == 1 ] && [ ! -z "$energy" ]; then
    runvenv "$port" "$machine" "$energy"
elif [ ! -z  "$port" ] &&  [ ! -z "$machine" ] &&  [ "$virtualenv" == 0 ] && [ ! -z "$energy" ]; then
    runnormal "$port" "$machine" "$energy"
fi
    
