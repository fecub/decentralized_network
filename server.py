from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from Monitoring import Monitoring
#from BatGui import BatteryGui
from battery import BatteryClass
import os
import sys
import argparse
import time
from threading import Thread

###### QT Import
#from PySide2.QtWidgets import QApplication
#from PySide2.QtCore import QObject,Property, Signal, Slot
#from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
##from utilsfolder.utils import text_type


PORT = None
MACHINEID = None
# 1=sun,2=wind,3=water,4=gas
ENERGYTYP = None
BATTERY = None

class EchoUDP(DatagramProtocol):
    global PORT, MACHINEID

    def __init__(self):
        pass

    def datagramReceived(self, p_datagram, address):
        if address[1] != PORT and MACHINEID not in p_datagram:
            datagram = str(p_datagram)
            
            if 'CRITIC_LOAD' in datagram or 'BATTERY_EMPTY' in datagram:
                self.send_request_to_specific(datagram, address)
                # if self.battery.get_battery_status() > 30:
                #     self.send_charge_battery(address)

            if 'LOAD_WEATHER' in datagram:
                etyp = int(datagram.split(',')[1])
                if(etyp == ENERGYTYP):
                    self.charge_via_energy(10)

    def send_request_to_specific(self, datagram, address):
        print address
        tmp=list(address)
        tmp[1] = int(datagram.split(',')[-1])
        address = tuple(tmp)
        print address
        if not BATTERY.charge_foreign_activ and not BATTERY.charge_own_activ and BATTERY.get_battery_status() > 30:
            self.transport.write("CHARGE_REQ,{0},{1}".format(MACHINEID, PORT), address)        

    
    # def send_charge_battery(self, address):
    #     print self.battery.get_battery_status()
    #     if self.battery.get_battery_status() > 30:
    #         print("request charging to " + str(address))
    #         if (self.charge_foreign_activ):
    #             self.transport.write("CHARGE,{0},6".format(MACHINEID), address )
    #             print "CHARGE foreign"
    #         else:
    #             self.transport.write("CHARGE_REQ,{0}".format(MACHINEID), address)
    #             print "CHARGE_REQ"
    #     else:
    #         self.charge_foreign_activ = False
    #         # self.charge_req_current = []
    #         self.transport.write("CHARGE_NOT", address)   
    #         del self.charge_req_current[:]
    #     time.sleep(2)
    
    def charge_via_energy(self, value):
        BATTERY.charge_without_limit(value)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Monitoring system for decentralized server')
    parser.add_argument('-p', '--port', type=str, help='Port number', required=True, nargs='+')
    parser.add_argument('-m', '--machine', type=str, help='machine id', required=True, nargs='+')
    parser.add_argument('-e', '--energy', type=str, help='Energy typ', required=True, nargs='+')
    parser.add_argument('-g', '--gui', help='Graphical User Interface', required=False, action='store_true' )
    args = parser.parse_args()

    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()

    

    PORT = int(args.port[0].split(",")[0])
    MACHINEID = args.machine[0]
    ENERGYTYP = int(args.energy[0].split(",")[0])

    print "Starting server."
    reactor.listenUDP(8000, EchoUDP())

    BATTERY = BatteryClass(PORT, MACHINEID)
    BATTERY.init_thread()

    print "Starting Client"
    mon = Monitoring(PORT, BATTERY)
    mon.start()
    
    if args.gui:
        # os.system("python desktop/main.py -e {0}".format(ENERGYTYP))
        import subprocess
        # subprocess.call(['python', 'desktop/main.py', '-e ' + str(ENERGYTYP)])
        subprocess.Popen('python desktop/main.py -e {0}'.format(str(ENERGYTYP)), shell=True)
    print "reactor.run"
    reactor.run()
