#!/usr/bin/venv python
from threading import Thread
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

from socket import SOL_SOCKET, SO_BROADCAST
from battery import BatteryClass
from random import randint
import argparse
import sys

class Monitoring(Thread):
    """docstring for Monitoring."""
    def __init__(self, port, battery):
        # self.protokoll = None
        self.battery = None
        self.port = port
        self.battery = battery
        # self.init_devices()
        Thread.__init__(self)
    
    # def init_devices(self):
    #     self.battery = BatteryClass(self.port, self.machineid) #randint(15, 20)
    #     self.battery.init_thread()

    def run(self):
        self.battery.start()
        # print self.port, self.battery
        t = reactor.listenUDP(self.port, self.battery)

# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(description='Monitoring system for decentralized server')
#     parser.add_argument('-p', '--port', type=str, help='Port number', required=True, nargs='+')
#     args = parser.parse_args()

#     mon = Monitoring(int(args.port[0].split(",")[0]))
#     mon.init_devices()
#     if len(sys.argv[1:]) == 0:
#         parser.print_help()
#         parser.exit()

#     mon.start()
