import sys

from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QObject,Property, Signal, Slot
from PySide2.QtQml import QQmlApplicationEngine, qmlRegisterType
#from utilsfolder.utils import text_type

from threading import Thread
import qt5reactor


class BatteryStatusProvider(QObject):
    batChanged=None
    def __init__(self, parent = None):
        QObject.__init__(self, parent)
        self._bat = None

    def getBat(self):
        return self._bat

    def setBat(self, value):
        self._bat = value
        self.on_batChanged.emit()

    on_batChanged = Signal()
    bat = Property(int, getBat, setBat, notify=on_batChanged)

class BatteryGui(Thread):
    """docstring for Monitoring."""
    def __init__(self):
        Thread.__init__(self)

    def init_devices(self):
        pass

    def run(self):
        self.app = QApplication(sys.argv)

        self.engine = QQmlApplicationEngine()
        self.engine.quit.connect(self.app.quit)
        self.btp = BatteryStatusProvider()

        self.engine.rootContext().setContextProperty("batteryStatus", self.btp)
        self.engine.load("qml/main.qml")
        self.btp.bat = 3
        qt5reactor.install()


        sys.exit(self.app.exec_())


#if __name__ == "__main__":
#    app = QApplication(sys.argv)

#    engine = QQmlApplicationEngine() #QQmlApplicationEngine("main.qml")
#    engine.quit.connect(app.quit)

#    btp = BatteryStatusProvider()

#    engine.rootContext().setContextProperty("batteryStatus", btp)
#    engine.load("main.qml")

#    btp.bat = 3

#    sys.exit(app.exec_())
