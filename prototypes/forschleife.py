#!/usr/bin/python
import time

SUM = 1530
LISTLED={}

def init():
    #GREEN  = Color(0, 255 ,0)
    #YELLOW  = Color(255, 255 ,0)
    #RED  = Color(255, 0 ,0)

    i=0
    for n in range(1, -1, -1):
        for c in range(255,5,-1):
            i=i+1
            LISTLED[i]=("GREEN", str(n), str(c))

    
    for n in range(1, -1, -1):
        for c in range(255,5,-1):
            i=i+1
            LISTLED[i]=("YELLOW", str(n), str(c))

    for n in range(1, -1, -1):
        for c in range(255,5,-1):
            i=i+1
            LISTLED[i]=("RED", str(n), str(c))
    
def show_led(percent):
    if percent<=0:
        print "[ERROR] Value must be greater than 0"
        return False
    _onep = len(LISTLED) / 100
    print _onep*percent, LISTLED[_onep*percent]

if __name__ == '__main__':
    # for i in range(100):
    init()
    show_led(1)
    # for i in range(100):
    #     show_led(i+1)

