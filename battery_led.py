#!/usr/bin/env python3
# Battery led indicator
# Author: Ferit Cubukcuoglu (tony@tonydicola.com)

import time
import os

from neopixel import *
import argparse

# LED strip configuration:
LED_COUNT      = 2      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 200     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LISTLED={}


strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, ws.WS2811_STRIP_GRB)
# Intialize the library (must be called once before other functions).
strip.begin()

def init():
    GREEN  = Color(0, 255 ,0)
    YELLOW  = Color(255, 255 ,0)
    RED  = Color(255, 0 ,0)

    i=0
    for n in range(2):
        for c in range(0, 255):
            i=i+1
            LISTLED[i]=(Color(c, 0 ,0), n, c, "r")
    
    for n in range(2):
        for c in range(0, 255):
            i=i+1
            LISTLED[i]=(Color(c, c ,0), n, c, "y")

    for n in range(2):
        for c in range(0, 255):
            i=i+1
            LISTLED[i]=(Color(0, c ,0), n, c, "g")
    
def setBatteryLevelPercent(percent):
    print percent
    if percent<=0:
        # print "[ERROR] Value must be greater than 0"
        return False
    _onep = len(LISTLED) / 100
    print _onep*percent, LISTLED[_onep*percent]

    led_element = LISTLED[_onep*percent]
    
    
    ledelement = led_element[0]
    if led_element[1] == 1:
        if led_element[3] in "r":
            strip.setPixelColor(0, Color(255,0,0))
        if led_element[3] in "y":
            strip.setPixelColor(0, Color(255,255,0))
        if led_element[3] in "g":
            strip.setPixelColor(0, Color(0,255,0))

    if led_element[1] == 0:
        strip.setPixelColor(1, Color(0,0,0))
        # if led_element[3] in "r":
        # if led_element[3] in "y":
        #     strip.setPixelColor(0, Color(255,255,0))
        # if led_element[3] in "g":
        #     strip.setPixelColor(0, Color(0,255,0))
    
    strip.setPixelColor(led_element[1], led_element[0])
    strip.show()


# Main program logic follows:
# if __name__ == '__main__':
#     # Process arguments
#     parser = argparse.ArgumentParser()
#     parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
#     args = parser.parse_args()

#     # Create NeoPixel object with appropriate configuration.
#     strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, ws.WS2811_STRIP_GRB)
#     # Intialize the library (must be called once before other functions).
#     strip.begin()

#     # Init battery list 
#     init()

#     # set battery level
#     # setBatteryLevelPercent(28)
#     for i in range(100, -1, -1):
#         setBatteryLevelPercent(i)
#         time.sleep(100/1000.0)


