from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

from socket import SOL_SOCKET, SO_BROADCAST
import os, time

class EchoClientDatagramProtocol(DatagramProtocol):
    def startProtocol(self):
        self.transport.socket.setsockopt(SOL_SOCKET, SO_BROADCAST, True)
        self.init_akku()

    # def init_akku(self):
    #     running = True
    #     while(running):
    #         self.sendDatagram()

    def sendDatagram(self):
        liveping = "i'm here: " + str(os.getpid())
        self.transport.write(liveping, ('255.255.255.255', 8000)) # <- write to broadcast address here
        time.sleep(1)


    def datagramReceived(self, datagram, host):
        print 'Datagram received: ', repr(datagram)
        self.sendDatagram()

def main():
    protocol = EchoClientDatagramProtocol()
    #0 means any port
    t = reactor.listenUDP(0, protocol)
    reactor.run()


if __name__ == '__main__':
   main()
