import QtQuick 2.11
import QtQml 2.11
import "BatteryDisplay"

Item {
    width: parent.width
    height: parent.height

    Rectangle {
        anchors.fill: parent

        Text {
            id: batico
            anchors.centerIn: parent
            text: changeload(batteryStatus.bat)
            font.family:Constants.fontRegularFamily
            font.pixelSize: 600
        }

    }

    function changeload(status) {
        // 5 status
        // battery-empty          uf244
        // battery-quarter        uf243
        // battery-half           uf242
        // battery-three-quarters uf241
        // battery-full           uf240

        if (status===1)
            batico.text = "\uf244"
        if (status===2)
            batico.text = "\uf243"
        if (status===3)
            batico.text = "\uf242"
        if (status===4)
            batico.text = "\uf241"
        if (status===5)
            batico.text = "\uf240"
    }
}
