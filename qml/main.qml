import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.4
import "BatteryDisplay"

ApplicationWindow {
    id: window
    width: Constants.width
    height: Constants.height
    visible: true
    title: "BatterDisplay"
//    visibility:"FullScreen"

    header: ToolBar {
        RowLayout {
            spacing: 20
            anchors.fill: parent

            Label {
                id: titleLabel
                text: "Battery display"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    DisplayBattery {
        id: displaybat
    }


    MouseArea {
        anchors.fill: parent
        onClicked: Qt.quit()
    }
}
