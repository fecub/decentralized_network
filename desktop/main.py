#!/usr/bin/python

import os
import sys
import pickle
import argparse
from PySide2.QtCore import QTimer, QUrl, Slot
from PySide2.QtGui import QGuiApplication
import PySide2.QtQml
from PySide2.QtQuick import QQuickView

@Slot()
def _check_value(rootobj):
    with open("batstat.p", "rb") as f:
        battery_load = int(pickle.load(f))
        status=0
        if (battery_load < 100 and battery_load > 90):
            status=5
        if (battery_load < 90 and battery_load > 60):
            status=4
        if (battery_load < 50 and battery_load > 30):
            status=3
        if (battery_load < 30 and battery_load > 10):
            status=2
        if (battery_load < 10 and battery_load >= 0):
            status=1

        rootobj.updateBattery(status, battery_load)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Monitoring system for battery')
    parser.add_argument('-e', '--energy', type=str, help='Energy typ', required=True, nargs='+')
    args = parser.parse_args()

    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()


    app = QGuiApplication(sys.argv)

    timer = QTimer()
    timer.start(1000)

    view = QQuickView()
    qmlFile = os.path.join(os.path.dirname(__file__), 'main.qml')
    view.setSource(QUrl.fromLocalFile(os.path.abspath(qmlFile)))
    if view.status() == QQuickView.Error:
        sys.exit(-1)
    root = view.rootObject()
    timer.timeout.connect(lambda:_check_value(root))
    
    root.updateEnergy(int(args.energy[0]))

    view.show()
    # view.showFullScreen()
    res = app.exec_()
    # Deleting the view before it goes out of scope is required to make sure all child QML instances
    # are destroyed in the correct order.
    del view
    sys.exit(res)
