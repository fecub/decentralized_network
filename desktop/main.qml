import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.4
import QtQuick.Window 2.0
import "BatteryDisplay"


Rectangle {
    id: window
    // width: Constants.width
    // height: Constants.height
    width: Screen.width/2
    height: Screen.height/2
    visible: true
    // title: "BatterDisplay"
    // visibility:"FullScreen"

    // header: ToolBar {
    //     RowLayout {
    //         spacing: 20
    //         anchors.fill: parent

    //         Label {
    //             id: titleLabel
    //             text: "Battery display"
    //             font.pixelSize: 20
    //             elide: Label.ElideRight
    //             horizontalAlignment: Qt.AlignHCenter
    //             verticalAlignment: Qt.AlignVCenter
    //             Layout.fillWidth: true
    //         }
    //     }
    // }

    DisplayBattery {
        id: displaybat
        onBatterychanged: {
            changeload(value, load)
        }
        onEnergychanged: {
            changeenergy(value)
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: Qt.quit()
    }

    function updateBattery(value, load) {
        displaybat.batterychanged(value, load)
    }

    function updateEnergy(value) {
        console.log("energychanged: "+value)
        
        displaybat.energychanged(value)
    }
}
