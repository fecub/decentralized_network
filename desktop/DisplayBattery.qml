import QtQuick 2.11
import QtQml 2.11
import "BatteryDisplay"
import QtQuick.Layouts 1.4

Item {
    width: parent.width
    height: parent.height
    signal batterychanged(int value, int load)
    signal energychanged(int value)

    Rectangle {
        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 0
        ColumnLayout {
            anchors.fill: parent
            spacing: 0
            Text {
                id: weatherlabel
                text: "NaN%"
                font.family: Constants.fontTextFamily
                font.pixelSize: 80
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                Layout.topMargin:60
            }
            Text {
                id: watherico
                text: changeenergy(4)
                font.family: Constants.fontWeatherFamily
                font.pixelSize: 300
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            }
            RowLayout {
                Layout.alignment: Qt.AlignHCenter
                spacing: 30
                Text {
                    id: battxt
                    text: "NaN%"
                    font.family:Constants.fontTextFamily
                    font.pixelSize: 100
                    font.bold: true
                }
                Text {
                    id: batico
                    text: changeload(1)
                    font.family:Constants.fontRegularFamily
                    font.pixelSize: 300
                }
            }
        }
    }

    function changeload(status, load) {
        // 5 status
        // battery-empty          uf244
        // battery-quarter        uf243
        // battery-half           uf242
        // battery-three-quarters uf241
        // battery-full           uf240

        // console.log(status)
        battxt.text = load + " %"

        if (status===1) {
            batico.text =  "\uf244"
            batico.color = "#ff0000"
            battxt.color = "#ff0000"
        }
        if (status===2) {
            batico.text =  "\uf243"
            batico.color = "#ff0000"
            battxt.color = "#ff0000"
        }
        if (status===3) {
            batico.text =  "\uf242"
            batico.color = "#FFA500"
            battxt.color = "#FFA500"
        }
        if (status===4) {
            batico.text =  "\uf241"
            batico.color = "#008000"
            battxt.color = "#008000"
        }
        if (status===5) {
            batico.text =  "\uf240"
            batico.color = "#008000"
            battxt.color = "#008000"
        }
    }

    function changeenergy(energytyp) {
        // 4 status
        // sun    uf244
        // wind   uf243
        // water  uf242
        // bio    uf241

        // console.log(energytyp)

        if (energytyp===1) {
            watherico.text =  "\uf00d"
            weatherlabel.text = "PHOTOVOLTAIK"
        }
        if (energytyp===2) {
            watherico.text =  "\uf050"
            weatherlabel.text = "WINDENERGIE"
        }
        if (energytyp===3) {
            watherico.text =  "\uf078"
            weatherlabel.text = "WASSERKRAFTWERK"
        }
        if (energytyp===4) {
            watherico.text =  "\uf079"
            weatherlabel.text = "BIOGAS"
        }
    }
}
